<?php 
session_start();
require_once("config.php");
?>


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
  integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
<div class="container-fluid ">

<style>

button {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 100%;
  margin-top: 5px;
}


body{
    background-image: url("/img/indexbg.jpg");
}


.card {
  border: none;
  margin: 0;
  width: 50%;
  height: 50%;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background: none;
  color: white;
}

* {
  margin: 0 auto;
}


</style>

  <div class="card" style="width: 45em;">
    <form action="addCallLog.php" method="post" name="addCallLog">
      <div class="row">
        <div class="col">
          <label>Call Date</label>
          <input type="date" class="form-control" name="callDate"  />
        </div>
      </div>
      <div class="row">
        <div class="col">
          <label>Call Time</label>
          <input type="time" class="form-control" name="callTime" />
        </div>
      </div>
      <div class="row">
        <div class="col">
          <label>Call Notes</label>
          <textarea type="string" class="form-control" name="callNote"></textarea>
        </div>
      </div>
      <br />
      <div class="row">
      <div class="col">
        <button class="btn btn-secondary" type="submit" name="submit">
          Submit
        </button>
        <button class="btn btn-secondary" type="submit" name="back">
          Back
        </button>
        
      </div>
      </div>
    </form>
  </div>
</div>
