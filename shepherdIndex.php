<?php
    session_start();
    require_once("config.php");
?>

<link
  rel="stylesheet"
  href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
  integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
  crossorigin="anonymous"
/>

<?php 
    if (isset($_POST['addProduction'])){
        header('Location:http://localhost:8000/shepherdProductionForm.php');
    }
    if (isset($_POST['viewProd'])){
      header('Location:http://localhost:8000/shepherdViewProduction.php');
  }
  if (isset($_POST['addBook'])){
    header('Location:http://localhost:8000/shepherdBookForm.php');
}
if (isset($_POST['viewBooks'])){
  header('Location:http://localhost:8000/shepherdViewBooks.php');
}

    

    if (isset($_POST['logout'])){
        header('Location:http://localhost:8000/abort.php');
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>


<style>
button{
    width: 120px;
}
body{
    background-image: url("/img/indexbg.jpg");
}

.navbar{
    background: none;
}

.color{
    color: white;
}

.go{
    float:right;
    margin-left: 40%;
}
</style>


<nav class="navbar navbar-expand-lg navbar-light bg-dark">
<div class="color">
<h3>Welcome, <?php echo $_SESSION['name']; ?> </h3>
<h6>Access Level :  <?php echo $_SESSION['isAdmin']; ?> </h6>
</div>
<div class="go">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item active">
      <form method="post" action="shepherdIndex.php">
                <button
                     class="btn btn-dark"
                        type="submit"
                        name="addProduction"
                         >
                         Add Prod
                    </button>
             </form>
      </li>
      <li class="nav-item active">
      <form method="post" action="shepherdIndex.php">
                <button
                     class="btn btn-dark"
                        type="submit"
                        name="addBook"
                         >
                        Add Book
                    </button>
             </form>
      </li>
      <li class="nav-item active">
      <form method="post" action="shepherdIndex.php">
                <button
                     class="btn btn-dark"
                        type="submit"
                        name="viewProd"
                         >
                         View Prod
                    </button>
             </form>
      </li>
      <li class="nav-item active">
      <form method="post" action="shepherdIndex.php">
                <button
                     class="btn btn-dark"
                        type="submit"
                        name="viewBooks"
                         >
                         View Books
                    </button>
             </form>
      </li>
      <li class="nav-item">
   
      <li class="nav-item">
       <form method="post" action="index.php">
                <button
                    class="btn btn-dark"
                    type="submit"
                    name="logout"
                    >
                   Log Out
                </button>
            </form>
      </li>
    </ul>
  </div>
  </div>
</nav>




<!-- 
<div class="division">
<div class="wrapper">
<div class="container-fluid">
  <div class="card" style="width: 50%;">
      <div class="row">
        <div class="text-center col">
         </div>
    </div>
    <div class="row">
         <div class="text-center col">
            <
        </div>
    </div>
    <div class="row">
         <div class="text-center col">
            <form method="post" action="index.php">
                <button
                    class="btn btn-dark"
                    type="submit"
                    name="addCallLog"
                     >
                    Add Call Log
                </button>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="text-center col">
            <form method="post" action="index.php">
                <button
                    class="btn btn-dark"
                    type="submit"
                    name="viewCallLog"
                    >
                    View Call Log
                </button>
            </form>
         </div>
    </div>
</div>
</div>
</div>
</div> -->



<!-- 
<h1>SUCCESS</h1>    

</body>
</html>

<form method="post" action="index.php">
<button
        class="btn btn-secondary"
        type="submit"
        name="addLead>"
      >
        Add Lead</button
      >
</form>

<h3>Welcome, <?php echo $_SESSION['empName']; ?> </h3>
<div class="container-fluid">
  <div class="card" style="width: 25em;">
    <form #userForm="ngForm" class="addForm" novalidate>
      <div class="row">
        <div class="col">
          <img src="https://icon-library.net//images/journal-icon/journal-icon-27.jpg" /><br />
        </div>
      </div>
      <div class="row">
        <div class="text-center col">
          <h4>Journal</h4>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <label>Username</label>
          <input type="string" class="form-control" name="email" #passUser />
        </div>
      </div>
      <div class="row">
        <div class="col">
          <label>Password</label>
          <input type="password" class="form-control" name="pass" #passPass />
        </div>
      </div>
      <br />
      <button
        class="btn btn-secondary"
        type="submit"
        (click)="onSubmit(passUser.value, passPass.value)"
      >
        Login</button
      ><br /><br />
      <section id="error" class="text-center col"><br /></section>
      <p class="text-center col" (click)="register()">
        Don't have an account yet? <u>Sign up</u> now!
      </p>
    </form>
  </div>
</div> -->
