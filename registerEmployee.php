<?php   
 
    require_once('config.php');
 
    $userId = $_POST['userId'];
    $username = $_POST['username'];
    $userpassword = password_hash($_POST['userpassword'],PASSWORD_BCRYPT);
    $salary = 15000;
    $empRole = 'Sales';
 
    $sql = 'INSERT INTO systemUser VALUES(?,?,?,?,?)';
 
    $statement = $dbConn -> prepare($sql);
 
    $result = $statement -> execute([$userId,$username,$userpassword, $salary, $empRole]);
 
    if($result > 0)
        header('Location:http://localhost:8000/empLogin.html');
    else
    {
        $message = "Duplicate Employee Id";
                    echo "<script type='text/javascript'>alert('$message');
                    window.location.href='registerEmployee.html';</script>";
    }