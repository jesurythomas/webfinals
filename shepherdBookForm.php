
<?php 
session_start();
require_once("config.php");
?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
  integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
<div class="container-fluid ">

<style>

button {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 100%;
  margin-top: 5px;
}


body{
    background-image: url("/img/indexbg.jpg");
}


.card {
  border: none;
  margin: 0;
  width: 50%;
  height: 50%;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background: none;
  color: white;
}

* {
  margin: 0 auto;
}


</style>

  <div class="card" style="width: 45em;">
    <form action="shepherdBook.php" method="post" name="shepheredProductionForm">
    <div class="row">
        <div class="col">
          <label>Book Id</label>
          <input type="text" class="form-control" name="bookId"  placeholder="Book ID"/>
        </div>
        <div class="col">
          <label>Author Id</label>
          <input type="text" class="form-control" name="authorId"  placeholder="authorId"/>
        </div>
     
      </div>
     
      <div class="row">
      <div class="col">
          <label>Book Title</label>
          <input type="text" class="form-control" name="bookTitle" placeholder="Book Title" />
        </div>
        </div>
       <div class="row">
       <div class="col">
          <label>Book Sub-Title</label>
          <input type="text" class="form-control" name="bookSubTitle"  placeholder="Book Sub-Title"/>
          </div>
       </div>
      <div class="row">
        <div class="col">
          <label>Number of Pages</label>
          <input type="number" class="form-control" name="numPages"  placeholder="Number Of Pages"/>
        </div>
        <div class="col">
          <label>ISBN</label>
          <input type="text" class="form-control" name="ISBN"  placeholder="ISBN"/>
        </div>
      </div>


      <br />
      <div class="row">
      <div class="col">
        <button class="btn btn-secondary" type="submit" name="submit">
          Submit
        </button>
        </div>
        <div class="col">
        <button class="btn btn-secondary" type="submit" name="back">
          Back
        </button>
        </div>
     
      </div>
    </form>
  </div>
</div>
