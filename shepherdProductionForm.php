
<?php 
session_start();
require_once("config.php");
?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
  integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
<div class="container-fluid ">

<style>

button {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 100%;
  margin-top: 5px;
}


body{
    background-image: url("/img/indexbg.jpg");
}


.card {
  border: none;
  margin: 0;
  width: 50%;
  height: 50%;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background: none;
  color: white;
}

* {
  margin: 0 auto;
}


</style>

  <div class="card" style="width: 45em;">
    <form action="shepherdProduction.php" method="post" name="shepheredProductionForm">
      <div class="row">
        <div class="col">
          <label>Prod Id</label>
          <input type="text" class="form-control" name="prodId"  placeholder="Prod Id"/>
        </div>
        <div class="col">
          <label>Book Id</label>
          <input type="text" class="form-control" name="bookId"  placeholder="Book Id"/>
        </div>
      </div>
    
    
      <div class="row">
        <div class="col">
          <label>Assigned To</label>
          <input type="text" class="form-control" name="assignedTo"  placeholder="Assigned To"/>
        </div>
        <div class="col">
      
          <label>Task Id</label>
         
          <td>   <select class="form-control" name="taskId">
            <option value="FBG">FBG</option>
            <option value="BKD">BKD</option>
            <option value="FSG">FSG</option>
            <option value="SCN">SCN</option>
            <option value="CVN">CVN</option>
            </select></td>
            
        </div>
      </div>


      <br />
      <div class="row">
      <div class="col">
        <button class="btn btn-secondary" type="submit" name="submit">
          Submit
        </button>
        </div>
        <div class="col">
        <button class="btn btn-secondary" type="submit" name="back">
          Back
        </button>
        </div>
     
      </div>
    </form>
  </div>
</div>
