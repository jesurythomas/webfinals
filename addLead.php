<?php 
            session_start();
            require_once("config.php");
 
            
            if(isset($_POST['back']))
            {
                header('Location:http://localhost:8000/index.php');
            }
            if(isset($_POST['submit']))
            {
                
                $firstName=$_POST['firstName'];
                $lastName=$_POST['lastName'];
                $authorssn=$_POST['authorssn'];
                $address1=$_POST['address1'];
                $address2=$_POST['address2'];
                $city=$_POST['city'];
                $statte=$_POST['statte'];
                $zipCode=$_POST['zipCode'];
                $phoneNumber=$_POST['phoneNumber'];
                $workPhone=$_POST['workPhone'];
                $emailAddress=$_POST['emailAddress'];
                $bestTime=$_POST['bestTime'];
                $empId= $_SESSION['employeeID'];
                $check=0;
 
                if(empty($firstName)||empty($lastName)||empty($authorssn)||empty($address1)||empty($address2)
                    ||empty($city)||empty($statte)||empty($zipCode)||empty($phoneNumber)||empty($workPhone)||empty($emailAddress)||empty($bestTime))
                    {
                            if(empty($firstName))
                            {
                                echo "<front color ='red'>First Name Field is empty.</font><br/>";
                            }
                            if(empty($lastName))
                            {
                                echo "<front color ='red'>Last Name Field is empty.</font><br/>";
                            }
                            
                            if(empty($authorssn))
                            {
                                echo "<front color ='red'>Author SSN Field is empty.</font><br/>";
                            }
                            if(empty($address1))
                            {
                                echo "<front color ='red'>Address 1  Field is empty.</font><br/>";
                            }
                          
                            if(empty($city))
                            {
                                echo "<front color ='red'>City  Field is empty.</font><br/>";
                            }
                            if(empty($statte))
                            {
                                echo "<front color ='red'>statte  Field is empty.</font><br/>";
                            }
                            
                            if(empty($zipCode))
                            {
                                echo "<front color ='red'>Zip Code  Field is empty.</font><br/>";
                            }
                            
                            if(empty($phoneNumber))
                            {
                                echo "<front color ='red'>Phone Number  Field is empty.</font><br/>";
                            }
                            
                         
                            if(empty($emailAddress))
                            {
                                echo "<front color ='red'>Email Address  Field is empty.</font><br/>";
                            }
                        
                            if(empty($bestTime))
                            {
                                echo "<front color ='red'>Email Address  Field is empty.</font><br/>";
                            }
                            echo"<br/><a href='javascript:self.history.back();'>Go Back</a>";
                        }
            
                    else
                    {
                        $dupesql = "SELECT * FROM salesPersonLeadDatabaseForm";
               
                        $query=$dbConn->prepare($dupesql);
                        $query->execute();
                        $row = $query ->fetchall(); 
                        foreach($row as $rows){
                     if ($rows['authorssn']==$authorssn){
                            $check=1;
                            echo "<front color ='red'> Duplicate Entry</font><br/>";
                            echo"<br/><a href='javascript:self.history.back();'>Go Back</a>";
                            break;
                            }
                       }
                       if ($check==0)
                       {
                            $sql = "INSERT INTO salesPersonLeadDatabaseForm(firstName, lastName, authorssn, address1, address2, city, statte, zipCode, phoneNumber, workPhone, emailAddress,
                             bestTime,empId) VALUES (:firstName, :lastName, :authorssn, :address1, :address2, :city, :statte, :zipCode, :phoneNumber, :workPhone, :emailAddress, :bestTime, :empId)";
                                $query=$dbConn->prepare($sql);

                                $query->bindParam(':firstName',$firstName);
                                $query->bindParam(':lastName',$lastName);
                                $query->bindParam(':authorssn',$authorssn);
                                $query->bindParam(':address1',$address1);
                                $query->bindParam(':address2',$address2);
                                $query->bindParam(':city',$city);
                                $query->bindParam(':statte',$statte);
                                $query->bindParam(':zipCode',$zipCode);
                                $query->bindParam(':phoneNumber',$phoneNumber);
                                $query->bindParam(':workPhone',$workPhone);
                                $query->bindParam(':emailAddress',$emailAddress);
                                $query->bindParam(':bestTime',$bestTime);
                                $query->bindParam(':empId',$empId);
                                $query->execute();

                                $message = "Successfully Added";
                                echo "<script type='text/javascript'>alert('$message');
                                window.location.href='http://localhost:8000/index.php';</script>";
 
                        
                    }

            }
        }

            