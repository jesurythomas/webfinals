<?php 
session_start();
require_once("config.php");
?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
  integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
<div class="container-fluid ">

<style>

button {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 100px;
  margin-top: 5px;
}


body{
    background-image: url("/img/indexbg.jpg");
}


.card {
  border: none;
  margin: 0;
  width: 10%;
  height: 50%;
  position: absolute;
  top: 45%;
  left: 50%;
  transform: translate(-50%, -50%);
  background: none;
  color: white;
}

* {
  margin: 0 auto;
}


</style>

  <div class="card" style="width: 45em;">
    <form action="addLead.php" method="post" name="addLeadForm">
      <div class="row">
        <div class="col">
          <label>First Name</label>
          <input type="text" class="form-control" name="firstName"  />
        </div>
        <div class="col">
          <label>Last Name</label>
          <input type="text" class="form-control" name="lastName" />
        </div>
      </div>

      <div class="row">
        <div class="col">
          <label>Address Line 1</label>
          <input type="text" class="form-control" name="address1" />
        </div>
        <div class="col">
          <label>Address Line 2</label>
          <input type="text" class="form-control" name="address2" />
        </div>
      </div>
      <div class="row">
        <div class="col">
          <label>City</label>
          <input type="text" class="form-control" name="city" />
        </div>
        <div class="col">
          <label>State</label>
          <input type="text" class="form-control" name="statte" />
        </div>
        <div class="col">
          <label>Zip Code</label>
          <input type="text" class="form-control" name="zipCode" />
        </div>
      </div>
      <div class="row">
        <div class="col">
          <label>Author SSN</label>
          <input type="number" class="form-control" name="authorssn" />
        </div>
        <div class="col">
          <label>Phone Number</label>
          <input type="number" class="form-control" name="phoneNumber" />
        </div>
        <div class="col">
          <label>Work Number</label>
          <input type="number" class="form-control" name="workPhone" />
        </div>
      </div>
      <div class="row">
        <div class="col">
          <label>Email</label>
          <input type="email" class="form-control" name="emailAddress" />
        </div>
        <div class="col">
          <label>Best Time to Call Back</label>
          <input type="text" class="form-control" name="bestTime" />
        </div>
      </div>
      <br />
      <div class="row">
      <div class="col-align-center">
        <button class="btn btn-secondary" type="submit" name="submit">
          Submit
        </button>
        <button class="btn btn-secondary" type="submit" name="back">
          Back
        </button>
        
      </div>
      </div>
    </form>
  </div>
</div>
