<?php
    session_start();
    
    $_SESSION['logged_in'] = false;
    $_SESSION['employeeID'];
    $_SESSION['isAdmin'];
    $_SESSION['name'];
    require_once('config.php');

    $searchSQL = "SELECT * FROM systemUser where empId = ?";
    $username = $_POST['username'];
    $userpassword = $_POST['userpassword'];

    $prepared = $dbConn->prepare($searchSQL);
    $prepared->execute (array($username));
    $result = $prepared->fetch(PDO::FETCH_ASSOC);

    if($result){
        if(password_verify($userpassword,$result['empPass']))
        {
        $_SESSION['isAdmin'] = $result['empRole'];
        if ( $_SESSION['isAdmin'] == 'Sales') 
        {
            $_SESSION['logged_in'] = true;
            $_SESSION['employeeID'] = $result['empID'];
            $_SESSION['name'] = $result['empName'];
            header('Location:http://localhost:8000/index.php');
        }
            else if  ( $_SESSION['isAdmin'] == 'Shepherd') {
                $_SESSION['logged_in'] = true;
                $_SESSION['employeeID'] = $result['empID'];
                $_SESSION['name'] = $result['empName'];
                header('Location:http://localhost:8000/shepherdIndex.php');
            }
            else if  ( $_SESSION['isAdmin'] == 'Admin') 
            {
                $_SESSION['logged_in'] = true;
                $_SESSION['employeeID'] = $result['empID'];
                $_SESSION['name'] = $result['empName'];
                header('Location:http://localhost:8000/adminIndex.php');
            }
            else
            {
                $_SESSION['logged_in'] = true;
                $_SESSION['employeeID'] = $result['empID'];
                $_SESSION['name'] = $result['empName'];
                header('Location:http://localhost:8000/index.php');
            }
        }
    else{
        header('Location:http://localhost:8000/empLogin.html');

        }
    }
    if (isset($_POST['registerEmployee'])){
        header('Location:http://localhost:8000/registerEmployee.html');
    }
    